package com.customer.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.customer.entity.CustomerTable;

/**
 * (CustomerTable)表数据库访问层
 *
 * @author makejava
 * @since 2020-08-20 11:24:26
 */
public interface CustomerTableDao extends BaseMapper<CustomerTable> {

}