package com.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.customer.entity.CustomerTable;

/**
 * (CustomerTable)表服务接口
 *
 * @author makejava
 * @since 2020-08-20 11:24:26
 */
public interface CustomerTableService extends IService<CustomerTable> {

}